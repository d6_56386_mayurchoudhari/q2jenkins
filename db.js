const mysql = require('mysql')

const openConnection = () =>{
    const connection =mysql.createConnection({
  
      
    // port: 3306,
        port: 3306,
        user: 'root',
        password: 'root',
        host: 'db',
       // database: 'sunbeam21'
        database: 'mydb'
    })

    connection.connect()

    return connection
}

module.exports={
    openConnection,
}